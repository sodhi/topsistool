/**
 * Copyright 2012 Balwinder Sodhi 
 * Permission is hereby granted under The MIT License
 * https://opensource.org/licenses/MIT
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package topsistool;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Balwinder Sodhi
 */
public class Types {

    /**
     * Represents a generic matrix. Element type is T.
     * @param <T> 
     */
    public static class Matrix<T> {

        public int cols;
        public int rows;
        private ArrayList<T> data;

        public Matrix(int rows, int cols) {
            this.cols = cols;
            this.rows = rows;
            this.data = new ArrayList<T>(cols * rows);
            this.data.addAll(Collections.<T>nCopies(cols * rows, null));
        }

        public T get(int row, int col) {
            return data.get(row * cols + col);
        }

        public void put(int row, int col, T num) {
            data.set(row * cols + col, num);
        }

        public void putColumn(ArrayList<T> colData, int colIndex) {
            for (int i = 0; i < rows; i++) {
                put(i, colIndex, colData.get(i));
            }
        }

        public ArrayList<T> getRow(int rowInd) {
            int start = rowInd * cols;
            return new ArrayList<T>(data.subList(start, start + cols));
        }

        public ArrayList<T> getColumn(int colInd) {
            ArrayList<T> colData = new ArrayList<T>();
            for (int i = 0; i < rows; i++) {
                colData.add(get(i, colInd));
            }
            return colData;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < rows; i++) {
                int start = cols * i;
                sb.append("\n");
                sb.append(data.subList(start, start + cols).toString());
            }
            sb.append("(").append(rows).append("x").append(cols).append(")\n");
            return sb.toString();
        }

        public void print() {
            System.out.println(toString());
        }
    }

    /**
     * Represents a triangular fuzzy number.
     */
    public static class TriangularFuzzyNum implements Comparable<TriangularFuzzyNum> {

        public double a1;
        public double a2;
        public double a3;

        public TriangularFuzzyNum(double a1, double a2, double a3) {
            this.a1 = a1;
            this.a2 = a2;
            this.a3 = a3;
        }

        public static TriangularFuzzyNum parse(String num) throws ParseException {
            String[] p = num.split(" ");
            return new TriangularFuzzyNum(Double.parseDouble(p[0].trim()),
                    Double.parseDouble(p[1].trim()), Double.parseDouble(p[2].trim()));
        }

        public double distance(TriangularFuzzyNum b) {
            double t = ((double) 1 / 3) * (Math.pow(a1 - b.a1, 2)
                    + Math.pow(a2 - b.a2, 2) + Math.pow(a3 - b.a3, 2));
            double v = Math.sqrt(t);
            return v;//Double.valueOf(String.format("%1.3f", v));
        }

        public int compareTo(TriangularFuzzyNum o) {
            if (this == o) {
                return 0;
            }
            if (a1 == o.a1 && a2 == o.a2 && a3 == o.a3) {
                return 0;
            }
            if (a1 < o.a1 && a2 <= o.a2 && a3 <= o.a3) {
                return -1;
            } else {
                return 1;
            }
        }

        @Override
        public String toString() {
            return String.format("(%1.3f, %2.3f, %3.3f)", a1, a2, a3);
        }
    }
}
