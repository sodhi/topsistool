/**
 * Copyright 2012 Balwinder Sodhi 
 * Permission is hereby granted under The MIT License
 * https://opensource.org/licenses/MIT
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package topsistool;

import java.util.Comparator;

/**
 * Custom triangular fuzzy number comparator for use in TOPSIS calculations.
 * 
 * @author Balwinder Sodhi
 */
public class MinMaxComparator implements Comparator<Types.TriangularFuzzyNum> {

    private boolean isMin;

    public MinMaxComparator(boolean isMin) {
        this.isMin = isMin;
    }

    public int compare(Types.TriangularFuzzyNum o1, Types.TriangularFuzzyNum o2) {
//        if (o1.getA1() == o2.getA1()) {
//            return 0;
//        }
        if (isMin) {
            if (o1.a1 < o2.a1) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (o1.a3 > o2.a3) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
