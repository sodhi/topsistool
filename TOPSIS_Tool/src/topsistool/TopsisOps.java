/**
 * Copyright 2012 Balwinder Sodhi 
 * Permission is hereby granted under The MIT License
 * https://opensource.org/licenses/MIT
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package topsistool;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This program implements the steps described for Fuzzy TOPSIS in the paper:
 * https://arxiv.org/abs/1205.5098 
 * (A Simplified Description of Fuzzy TOPSIS. By Balwinder Sodhi, Prabhakar T.V)
 * @author Balwinder Sodhi
 */
public class TopsisOps {

    private ConfigParser config;

    public TopsisOps(String inputFile) throws IOException, ParseException {
        config = new ConfigParser(inputFile);
        System.out.println("================================ START OF INPUT DATA");
        config.print();
        System.out.println("================================== END OF INPUT DATA");
    }

    /**
     * Checks whether the type of criteria at an index is cost type.
     * @param index Index of the criteria type in config file.
     * @return true if criteria is cost type, else false.
     */
    private boolean isCriterionCostType(int index) {
        String v = config.getCritTypes().get(index);
        return "C".equalsIgnoreCase(v);
    }

    /**
     * Performs the TOPSIS steps on the input data as loaded from the CSV
     * file supplied to the program. Output is printed on the console.
     * @throws IOException
     * @throws ParseException 
     */
    public void doTopsis() throws IOException, ParseException {

        // Build the aggregated fuzzy DM
        Types.Matrix<Types.TriangularFuzzyNum> fuzzyDM = calcAggrFuzzyDM();
        System.out.println("In all the matrices below, the element M[i][j] = "
                + "value for alternative-i for criterion-j");
        System.out.println("Aggregate Fuzzy DM (Alt. x Crit.): "+fuzzyDM);
        normalizeFuzzyDM(fuzzyDM);
        System.out.println("Normalized aggregate Fuzzy DM (Alt. x Crit.): "+fuzzyDM);
        calcWeightedNormalizedFuzzyDM(fuzzyDM);
        System.out.println("Weighted normalized aggregate Fuzzy DM (Alt. x Crit.): "+fuzzyDM);
        calcDistnacesFromIdealSolns(fuzzyDM);
    }
    
    /**
     * Input matrix looks like: 
     * (Rows for alternatives and columns for decision makers)
     *     DM1   DM2  DM3
     * A1  n11   n12  n13
     * A2  n21   n22  n23
     * A3  n31   n32  n33
     *
     * 
     * @param mat
     * @return 
     */
    private ArrayList<Types.TriangularFuzzyNum> aggregateFuzzyWeights(
            Types.Matrix<Types.TriangularFuzzyNum> mat) {
        
        ArrayList<Types.TriangularFuzzyNum> aggr = 
                new ArrayList<Types.TriangularFuzzyNum>();
        for (int i = 0; i < mat.rows; i++) {
            ArrayList<Types.TriangularFuzzyNum> row = mat.getRow(i);
            Collections.sort(row, new MinMaxComparator(true));
            double minA = row.get(0).a1;
            Collections.sort(row, new MinMaxComparator(false));
            double maxC = row.get(0).a3;
            double mid = 0;
            for (int j = 0; j < row.size(); j++) {
                mid += row.get(j).a2;
            }
            mid = mid / row.size();
            aggr.add(new Types.TriangularFuzzyNum(minA, mid, maxC));
        }
        //System.out.println("\nAggregate for matrix "+mat+" is: "+aggr);
        return aggr;
    }

    /**
     * Calculates the aggregated fuzzy decision matrix.
     * @return
     * @throws IOException 
     */
    private Types.Matrix<Types.TriangularFuzzyNum> calcAggrFuzzyDM() 
            throws IOException {
        /**
         * Each element of this list contains the ratings given by decision
         * makers for the alternatives. Each element of the list holds the
         * ratings matrix for one criterion.
         * 
         * (Rows for alternatives and columns for decision makers)
         *     DM1   DM2  DM3
         * A1  n11   n12  n13
         * A2  n21   n22  n23
         * A3  n31   n32  n33
         */
        ArrayList<Types.Matrix<Types.TriangularFuzzyNum>> altRatingsForCrit = 
                config.getAltRatings();        
        int rows = altRatingsForCrit.get(0).rows;
        int cols = altRatingsForCrit.size();
        Types.Matrix fuzzyDM = new Types.Matrix(rows, cols);
        for (int i = 0; i < altRatingsForCrit.size(); i++) {
            /**
             * Contains the aggregated ratings for each alternative for the
             * criterion C[i].
             */
            ArrayList<Types.TriangularFuzzyNum> aggrFuzzyWt = 
                    aggregateFuzzyWeights(altRatingsForCrit.get(i));
            fuzzyDM.putColumn(aggrFuzzyWt, i);
        }
        return fuzzyDM;
    }

    /**
     * Updated the input fuzzy decision matrix such that the element values
     * represent normalized fuzzy decision ratings.
     * @param mat Input matrix to be updated.
     */
    private void normalizeFuzzyDM(Types.Matrix<Types.TriangularFuzzyNum> mat) {
        for (int i = 0; i < mat.cols; i++) {
            ArrayList<Types.TriangularFuzzyNum> colData = mat.getColumn(i);
            double a=0,b=0,c=0;
            if (isCriterionCostType(i)) {
                Collections.sort(colData, new MinMaxComparator(true));
                double a_ = colData.get(0).a1;
                for (int j = 0; j < colData.size(); j++) {
                    Types.TriangularFuzzyNum tfn = colData.get(j);
                    a = tfn.a1; b = tfn.a2; c = tfn.a3;
                    tfn.a1 = (a_/c);
                    tfn.a2 = (a_/b);
                    tfn.a3 = (a_/a);
                }
            } else {
                Collections.sort(colData, new MinMaxComparator(false));
                double c_ = colData.get(0).a3;
                //System.out.println("ColData="+colData+"\t >>> c_ = "+c_);
                for (int j = 0; j < colData.size(); j++) {
                    Types.TriangularFuzzyNum tfn = colData.get(j);
                    a = tfn.a1; b = tfn.a2; c = tfn.a3;
                    tfn.a1 = (a/c_);
                    tfn.a2 = (b/c_);
                    tfn.a3 = (c/c_);
                }
            }
        }
    }

    /**
     * Input fuzzy decision matrix is updated such that the elements now
     * represent the corresponding weighted values.
     * @param mat Input fuzzy decision matrix to be updated.
     * @throws IOException 
     */
    private void calcWeightedNormalizedFuzzyDM(
            Types.Matrix<Types.TriangularFuzzyNum> mat) throws IOException {
        
        Types.Matrix<Types.TriangularFuzzyNum> critWtsRatings = 
                config.getCritWeightage();
        System.out.println("\nCriteria weightage ratings (W[i][j] = "
                + "Weightage for ith criterion given by jth DM):"+critWtsRatings);
        ArrayList<Types.TriangularFuzzyNum> wtdAggrCritRatings = 
                aggregateFuzzyWeights(critWtsRatings);
        System.out.println("\nAggregated criteria weightage ratings "
                + "(W[i] = Weightage for ith criterion):\n"+wtdAggrCritRatings+"\n\n");
        for (int i = 0; i < mat.rows; i++) {
            ArrayList<Types.TriangularFuzzyNum> rowData = mat.getRow(i);
            //System.out.println(">>>> ROW "+i+": "+rowData);
            for (int j = 0; j < rowData.size(); j++) {
                Types.TriangularFuzzyNum rating = rowData.get(j);
                Types.TriangularFuzzyNum wt = wtdAggrCritRatings.get(j);
                //System.out.println(">> Rating="+rating+"\tWt="+wt);
                rating.a1 = (rating.a1 * wt.a1);
                rating.a2 = (rating.a2 * wt.a2);
                rating.a3 = (rating.a3 * wt.a3);
            }
        }
    }

    /**
     * 
     * @param mat 
     */
    private void calcDistnacesFromIdealSolns(
            Types.Matrix<Types.TriangularFuzzyNum> mat) {

        Types.Matrix<Double> fnisDistMat = new Types.Matrix<Double>(mat.rows, 
                mat.cols);
        Types.Matrix<Double> fpisDistMat = new Types.Matrix<Double>(mat.rows, 
                mat.cols);

        for (int i = 0; i < mat.cols; i++) {
            ArrayList<Types.TriangularFuzzyNum> colData = mat.getColumn(i);
            // Find the FNIS
            Collections.sort(colData, new MinMaxComparator(true));
            double minA = colData.get(0).a1;
            Types.TriangularFuzzyNum fnis = new Types.TriangularFuzzyNum(minA, 
                    minA, minA);
            //System.out.println(">>>FNIS="+fnis);

            // Find the FPIS
            Collections.sort(colData, new MinMaxComparator(false));
            double maxC = colData.get(0).a3;
            Types.TriangularFuzzyNum fpis = new Types.TriangularFuzzyNum(maxC,
                    maxC, maxC);
            //System.out.println(">>>FNPS="+fpis);
            // Reaload the columns data for distance calculation
            colData = mat.getColumn(i);
            for (int j = 0; j < colData.size(); j++) {
                Types.TriangularFuzzyNum tfn = colData.get(j);
                double dn = tfn.distance(fnis);
                double dp = tfn.distance(fpis);
                dn = round(dn, 3);
                dp = round(dp, 3);
                fnisDistMat.put(j, i, dn);
                fpisDistMat.put(j, i, dp);
            }
        }
        System.out.println("FPIS distances matrix (Alt. x Crit.): "+fpisDistMat);
        System.out.println("FNIS distances matrix (Alt. x Crit.): "+fnisDistMat);
        ArrayList<Double> fnisSums = new ArrayList<Double>();
        ArrayList<Double> fpisSums = new ArrayList<Double>();
        ArrayList<Double> cc = new ArrayList<Double>();

        for (int i = 0; i < fnisDistMat.rows; i++) {
            ArrayList<Double> fnisRowDat = fnisDistMat.getRow(i);
            ArrayList<Double> fpisRowDat = fpisDistMat.getRow(i);
            double sumFnis=0;
            double sumFpis=0;
            for (int j = 0; j < fnisRowDat.size(); j++) {
                sumFnis += fnisRowDat.get(j);
                sumFpis += fpisRowDat.get(j);
            }
            fnisSums.add(sumFnis);
            fpisSums.add(sumFpis);
            cc.add(round(sumFnis / (sumFpis + sumFnis), 3));
        }

        System.out.println("In the vectors below, value V[i] is for alternative-i");
        System.out.println("FNIS Sums: "+fnisSums);
        System.out.println("FPIS Sums: "+fpisSums);
        System.out.println("CC       : "+cc);
        
        ArrayList<Double> sorted = new ArrayList<Double>(cc);
        Collections.sort(sorted);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sorted.size(); i++) {
            sb.append("A").append(cc.indexOf(sorted.get(i)));
            if (i < sorted.size()-1) sb.append(" < ");
        }
        System.out.println("Alternatives from worst to best: "+ sb);
    }

    private double round(double num, int decimals) {
        double mult = Math.pow(10, decimals);
        int dd = (int) (mult * num);
        return dd/mult;
    }
    
}
