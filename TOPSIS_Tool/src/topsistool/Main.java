/**
 * Copyright 2012 Balwinder Sodhi Permission is hereby granted under The MIT
 * License https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package topsistool;

/**
 *
 * @author Balwinder Sodhi
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        StringBuilder msg = new StringBuilder();
        msg.append("\nUsage: java ").append(Main.class.getName());
        msg.append(" <Input CSV file>");

        if (args.length < 1) {
            System.err.println(msg);
            return;
        }
        try {
            TopsisOps t = new TopsisOps(args[0]);
            t.doTopsis();
        } catch (Exception e) {
            System.out.println("Error while processing: "+e.getMessage());
        }
        System.out.println("====================================\n");
        //Topsis.printMatrix(t.getFuzzyDM());
    }

}
