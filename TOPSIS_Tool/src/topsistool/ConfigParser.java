/**
 * Copyright 2012 Balwinder Sodhi 
 * Permission is hereby granted under The MIT License
 * https://opensource.org/licenses/MIT
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package topsistool;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Parser for input CSV file. Please see the sample input file for description
 * of the format of input.
 * 
 * @author Balwinder Sodhi
 */
public class ConfigParser {

    private ArrayList<Types.Matrix<Types.TriangularFuzzyNum>> altRatings = 
            new ArrayList<Types.Matrix<Types.TriangularFuzzyNum>>();
    private Types.Matrix<Types.TriangularFuzzyNum> critWeightage;
    private HashMap<String, Types.TriangularFuzzyNum> ratingTerms = 
            new HashMap<String, Types.TriangularFuzzyNum>();
    private ArrayList<String> critTypes = new ArrayList<String>();

    private int critCount = 0;
    private int altCount = 0;
    private int dmCount = 0;
    private int currentRecord=0;

    private String inputFile;
    public ConfigParser(String inputFile) throws IOException, ParseException {

        this.inputFile = inputFile;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(inputFile));
            String line =null;
            while ((line = br.readLine()) != null) {
                //System.out.println("Read line: "+line);
                if (line.startsWith("$COUNTS")) {
                    critCount = Integer.parseInt(line.split(",")[1]);
                    altCount = Integer.parseInt(line.split(",")[2]);
                    dmCount = Integer.parseInt(line.split(",")[3]);
                }
            }
        } finally {
            if (br != null) br.close();
        }

        critWeightage = new Types.Matrix<Types.TriangularFuzzyNum>(critCount, dmCount);
        parseCSV();
    }

    public void print() {
        System.out.println("Alternatives count: "+this.altCount);
        System.out.println("Criteria count: "+this.critCount);
        System.out.println("Decision makers: "+this.dmCount);
        System.out.println("Criteria types: "+this.critTypes);
        System.out.println("Rating terms: "+this.ratingTerms);
        System.out.println("Criteria weightage (Crit. x DM): "
                +this.critWeightage);
        System.out.println("Alternative ratings on criteria (Crit. x [Alt. x DM]):"+
                this.altRatings);
    }

    public int getAltCount() {
        return altCount;
    }

    public ArrayList<Types.Matrix<Types.TriangularFuzzyNum>> getAltRatings() {
        return altRatings;
    }

    public int getCritCount() {
        return critCount;
    }

    public ArrayList<String> getCritTypes() {
        return critTypes;
    }

    public Types.Matrix<Types.TriangularFuzzyNum> getCritWeightage() {
        return critWeightage;
    }

    public int getDmCount() {
        return dmCount;
    }

    public HashMap<String, Types.TriangularFuzzyNum> getRatingTerms() {
        return ratingTerms;
    }

    
    private void processLine(Section section, String line) throws ParseException {

        switch(section) {
            case AltRatings:
                loadAltRating(line);
                break;
            case CritTypes:
                loadCritType(line);
                break;
            case CritWeights:
                loadCritWeightage(line);
                break;
            case RatingTerms:
                loadLinguTerm(line);
                break;
            case WeightTerms:
                loadLinguTerm(line);
                break;
        }
    }

    private void loadAltRating(String line) {
        String[] p = line.split(",");
        Types.Matrix mat = new Types.Matrix(altCount, dmCount);
        for (int i = 0; i < altCount; i++) {
            String[] dmRtgs = p[i + 2].split(" ");
            for (int j = 0; j < dmRtgs.length; j++) {
                mat.put(i, j, ratingTerms.get(dmRtgs[j]));
            }
        }
        altRatings.add(mat);
    }

    private void loadCritWeightage(String line) {
        String[] p = line.split(",");
        for (int i = 0; i < dmCount; i++) {
            critWeightage.put(currentRecord, i, ratingTerms.get(p[i + 2]));
        }
    }

    private void loadLinguTerm(String line) throws ParseException {
        String[] p = line.split(",");
        Types.TriangularFuzzyNum fn = Types.TriangularFuzzyNum.parse(p[2]);
        ratingTerms.put(p[1], fn);
    }

    private void loadCritType(String line) {
        String[] p = line.split(",");
        critTypes.add(p[2]);
    }

    private enum Section {
        RatingTerms, WeightTerms, CritTypes, CritWeights, AltRatings, None
    }
    
    private void parseCSV() throws IOException, ParseException {

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line = null;
        Section ss = Section.None;
        try {
            while ((line = br.readLine()) != null) {
                //System.out.println("Read line: "+line);
                if (line.startsWith("#")) continue;
                if (line.startsWith(";")) {
                    ss = Section.None;
                    currentRecord=0;
                    continue;
                }
                if (line.startsWith("$TERMS_FOR_RATINGS")) {
                    ss = Section.RatingTerms;

                } else if (line.startsWith("$TERMS_FOR_WEIGHTS")) {
                    ss = Section.WeightTerms;

                } else if (line.startsWith("$CRIT_TYPES")) {
                    ss = Section.CritTypes;

                } else if (line.startsWith("$CRIT_WEIGHTAGE")) {
                    ss = Section.CritWeights;

                } else if (line.startsWith("$ALT_RATINGS")) {
                    ss = Section.AltRatings;

                } else {
                    processLine(ss, line.toUpperCase());
                    currentRecord++;
                }
            }            
        } finally {
            br.close();
        }
    }
}
