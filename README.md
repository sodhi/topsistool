# README #
This project implements a Java program which performs the steps involved in multi-criteria decision making process using Fuzzy TOPSIS method. It implements the Fuzzy TOPSIS steps as described in this paper:
Balwinder Sodhi and Prabhakar T.V. "A Simplified Description of Fuzzy TOPSIS" https://arxiv.org/abs/1205.5098